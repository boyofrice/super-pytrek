from kivy.uix.image import Image
from random import seed, randint

__author__ = 'Chris'

"""
EXPLOSIONS!!!
"""


class Explosion(Image):
    def __init__(self, **kwargs):
        # making this stuff here cause dynamically loading image
        super(Explosion, self).__init__(**kwargs)
        seed()
        self.timer = 1
        self.active = False
        explosion_number = randint(0, 2)
        self.source = 'assets/explosion' + str(explosion_number) + '.png'
        self.width = 200
        self.height = 200

    # simple countdown stuff
    def update(self, dt):
        self.timer -= dt
        if self.timer <= 0:
            self.active = False