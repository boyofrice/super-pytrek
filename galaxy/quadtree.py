from kivy.uix.widget import Widget

__author__ = 'Chris'

quad_pool = []


def get_quad_tree():
    for q in quad_pool:
        if not q.active:
            q.active = True
            return q

    return_quad = QuadTree()
    quad_pool.append(return_quad)
    return return_quad


class QuadTree(Widget):
    def __init__(self, **kwargs):
        super(QuadTree, self).__init__(**kwargs)
        self.northwest = None
        self.northeast = None
        self.southwest = None
        self.southeast = None
        self.points = []
        self.node_cap = 4
        self.depth = 8
        self.active = True

    def insert(self, p):
        if not self.collide_widget(p):
            return False

        if len(self.points) < self.node_cap:
            self.points.append(p)
            return True

        if self.northwest is None:
            self.subdivide()

        if self.northwest.insert(p):
            return True
        if self.northeast.insert(p):
            return True
        if self.southeast.insert(p):
            return True
        if self.southwest.insert(p):
            return True

        return False

    def subdivide(self):
        half_size = self.width / 2, self.height / 2
        self.northwest = get_quad_tree()
        self.northwest.x = self.x
        self.northwest.y = self.center_y
        self.northwest.size = half_size

        self.northeast = get_quad_tree()
        self.northeast.x = self.center_x
        self.northeast.y = self.center_y
        self.northeast.size = half_size

        self.southwest = get_quad_tree()
        self.southwest.x = self.x
        self.southwest.y = self.y
        self.southwest.size = half_size

        self.southeast = get_quad_tree()
        self.southeast.x = self.center_x
        self.southeast.y = self.y
        self.southeast.size = half_size

    def query(self, p):
        points_in_range = []

        if not self.collide_widget(p):
            return points_in_range

        for obj in self.points:
            if p.collide_widget(obj):
                points_in_range.append(obj)

        if self.northwest is None:
            return points_in_range

        points_in_range.extend(self.northwest.query(p))
        points_in_range.extend(self.northeast.query(p))
        points_in_range.extend(self.southwest.query(p))
        points_in_range.extend(self.southeast.query(p))

        return list(set(points_in_range))

    def clean_up(self):
        self.points = []
        if self.northwest is not None:
            self.northwest.clean_up()
            self.northwest.active = False
            self.northwest = None
            self.northeast.clean_up()
            self.northeast.active = False
            self.northeast = None
            self.southeast.clean_up()
            self.southeast.active = False
            self.southeast = None
            self.southwest.clean_up()
            self.southwest.active = False
            self.southwest = None
        self.active = False