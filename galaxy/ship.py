from kivy.uix.widget import Widget
from kivy.vector import Vector
from kivy.core.audio import SoundLoader
from kivy.animation import Animation

__author__ = 'Chris'

"""
This is the player's ship
"""


class Ship(Widget):
    timer = 0.0
    fire_rate = 0.4
    shot_sound = SoundLoader.load('assets/shot.wav')
    health = 100

    def on_touch_move(self, touch):
        self.center = touch.pos

    def update(self, dt):
        # shoot at the fire rate
        self.timer += dt
        if self.timer >= self.fire_rate:
            # shoot something
            self.timer = 0.0
            self.shot_sound.play()
            shot_pos = Vector(self.center_x, self.top)
            self.parent.shoot_things(shot_pos)
