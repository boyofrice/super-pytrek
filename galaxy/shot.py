from kivy.uix.image import Image

__author__ = 'Chris'


class Shot(Image):
    def __init__(self, **kwargs):
        super(Shot, self).__init__(**kwargs)
        self.active = False
