from kivy.uix.widget import Widget
from kivy.uix.image import Image
from random import seed, randint
from galaxy.enemy import Enemy
from kivy.properties import ObjectProperty

__author__ = 'Chris'

"""
The Map is responsible for creating the star field and scrolling it across the screen
"""


class Map(Widget):
    star_field_1 = ObjectProperty(None)
    star_field_2 = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Map, self).__init__(**kwargs)
        self.timer = 0.0
        self.enemy_pool = [Enemy(), Enemy(), Enemy()]
        self.stars = []
        self.enemies = []
        self.enemy_spawn_timer = 2.0

    # randomly creates stars in places you'll never know
    def generate_stars(self):
        self.stars = [self.star_field_1, self.star_field_2]

    def get_enemy(self):
        # search the enemy pool for an inactive enemy
        for e in self.enemy_pool:
            if not e.active:
                return e

        # none available, make a new one
        return_enemy = Enemy()
        self.enemy_pool.append(return_enemy)

        return return_enemy

    # updates the galaxy map each frame
    def update(self, dt):
        self.timer += dt
        # update stars
        for star in self.stars:
            # move each star down
            star.center_y -= 10
            # if it's not visible move it to the top of the screen
            if not 0 <= star.center_x <= self.width or star.top <= 0:
                star.y = self.top

        # attempt to spawn enemies
        if self.timer >= self.enemy_spawn_timer:
            self.timer = 0.0
            enemy = self.get_enemy()
            enemy.active = True
            enemy.center_x = randint(0, self.right)
            enemy.center_y = self.top + 100
            self.enemies.append(enemy)
            self.add_widget(enemy)

        for enemy in self.enemies:
            enemy.center_y -= 1
            if enemy.top < 0:
                enemy.active = False
                self.enemies.remove(enemy)
                self.remove_widget(enemy)
