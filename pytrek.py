from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, NumericProperty
from kivy.core.audio import SoundLoader
from map import Map
from galaxy.ship import Ship
from galaxy.explosion import Explosion
from random import seed, randint
from title import Title
from galaxy.quadtree import get_quad_tree
from galaxy.shot import Shot

__author__ = 'Chris'

"""
PyTrek Game is the root widget of the game.
"""


class PyTrekGame(Widget):
    player = ObjectProperty(None)
    map = ObjectProperty(None)
    title_screen = ObjectProperty(None)
    thingies = []  # list of thingies to update
    shooties = []  # cool stuff
    explosions = []  # the question to all answers
    explosion_sounds = [
        SoundLoader.load('assets/exp0.wav'),
        SoundLoader.load('assets/exp1.wav'),
        SoundLoader.load('assets/exp2.wav')
    ]
    score = NumericProperty(0)
    title = True

    def __init__(self, **kwargs):
        super(PyTrekGame, self).__init__(**kwargs)
        self.music = SoundLoader.load('assets/megawall.ogg')
        seed()
        self.shot_pool = [Shot(), Shot(), Shot(), Shot()]
        self.explosion_pool = [Explosion(), Explosion(), Explosion(), Explosion()]

    def new_game(self):
        """
        initializes a new game
        """
        self.title = False
        self.remove_widget(self.title_screen)
        self.map.generate_stars()
        self.thingies.append(self.player)
        self.thingies.append(self.map)
        self.music.loop = True
        self.music.play()

    def get_shot(self):
        # search the shot pool for an inactive shot
        for s in self.shot_pool:
            if not s.active:
                return s

        # none available, make a new one
        return_shot = Shot()
        self.shot_pool.append(return_shot)
        return return_shot

    def get_explosion(self):
        for e in self.explosion_pool:
            if not e.active:
                e.timer = 1
                e.active = True
                return e

        return_explosion = Explosion()
        return_explosion.active = True
        self.explosion_pool.append(return_explosion)
        return return_explosion

    def update(self, dt):
        """
        main update loop for the game

        @param dt: delta time, the time passed between frame updates
        """
        if not self.title:
            # UPDATE ALL THE THINGIES
            for thingy in self.thingies:
                thingy.update(dt)
            for awesome in self.shooties:
                awesome.center_y += 4
                if awesome.y > self.top:
                    awesome.active = False
                    self.remove_widget(awesome)
                    self.shooties.remove(awesome)
            self.check_shots()
            self.check_player()
            for exp in self.explosions:
                exp.update(dt)
                if not exp.active:
                    self.explosions.remove(exp)
                    self.remove_widget(exp)

    # shoots things
    def shoot_things(self, pos):
        # make a bullet or something
        death_carrot = self.get_shot()
        death_carrot.center = pos
        death_carrot.active = True
        self.shooties.append(death_carrot)
        self.add_widget(death_carrot)

    def check_shots(self):
        # build quad tree of enemies
        quad_tree = get_quad_tree()
        quad_tree.pos = self.pos
        quad_tree.size = self.size
        for bad_dude in self.map.enemies:
            quad_tree.insert(bad_dude)

        for awesome in self.shooties:
            shot_removed = False
            for dude in quad_tree.query(awesome):
                if not shot_removed:
                    awesome.active = False
                    self.shooties.remove(awesome)
                    self.remove_widget(awesome)
                    shot_removed = True
                self.do_awesome(dude)

        # done with quad tree
        quad_tree.clean_up()

    def do_awesome(self, enemy):
        """
        kills and enemy and puts and explosion in their place

        @param enemy: the enemy to destroy
        @type enemy: galaxy.enemy.Enemy
        """
        enemy.active = False
        self.map.enemies.remove(enemy)
        self.map.remove_widget(enemy)
        self.score += 10
        self.make_explosion(enemy.center)
        if self.map.enemy_spawn_timer > 0.8:
            self.map.enemy_spawn_timer -= 0.05

    def make_explosion(self, here):
        exp = self.get_explosion()
        exp.center = here
        self.explosions.append(exp)
        self.add_widget(exp)
        exp_sound_index = randint(0, len(self.explosion_sounds) - 1)
        self.explosion_sounds[exp_sound_index].play()

    def check_player(self):
        # build quad tree of enemies
        quad_tree = get_quad_tree()
        quad_tree.pos = self.pos
        quad_tree.size = self.size
        for bad_dude in self.map.enemies:
            quad_tree.insert(bad_dude)

        for dude in quad_tree.query(self.player):
            self.do_awesome(dude)
            self.player.health -= 10

        if self.player.health <= 0:
            self.make_explosion(self.player.center)
            self.player.health = 100
            self.player.center = self.center_x, self.y

        quad_tree.clean_up()