from kivy.app import App
from kivy.clock import Clock
from pytrek import PyTrekGame

__author__ = 'Chris'

"""
Application entry point
"""


class PyTrekApp(App):
    def build(self):
        game = PyTrekGame()
        game.new_game()
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game


if __name__ == '__main__':
    PyTrekApp().run()
